package com.esites.playtest.sdk;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import javax.inject.Inject;

/**
 * Created by tnanlohy on 07/06/16.
 */
public class IdentificationDialogFragment extends DialogFragment implements View.OnClickListener {

    @Inject
    PreferenceService mPreferenceService;


    private static final int    REQ_CODE_LOGIN_SIGNUP = 998;
    private static final String SIGNUP_URL            = "android/v2/signup";
    private static final String LOGIN_URL             = "android/v2/login";
    private static final String TAG                   = "IdentificationDialog";

    public interface Listener {
        void onSuccess();
        void onCancelled();
    }

    private Listener mListener;
    private boolean mSuccess = false;

    public static IdentificationDialogFragment createInstance(Listener listener) {
        IdentificationDialogFragment fragment = new IdentificationDialogFragment();
        fragment.setListener(listener);
        return fragment;
    }

    private void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogSlideAnim);

        Playtest.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_identification, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.sign_up).setOnClickListener(this);
        view.findViewById(R.id.login).setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title and set background to transparent
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);

        return dialog;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Context context = v.getContext();

        // Create Intent for WebviewActivity
        Intent intent = new Intent(context, WebviewActivity.class);
        String which = LOGIN_URL;
        if (id == R.id.sign_up) {
            which = SIGNUP_URL;
        }

        String userUuid = mPreferenceService.getDeviceUuid();
        String addOn = "?idfa=" + userUuid + "&platform=android";
        String url = BuildConfig.BASE_URL + which + addOn;
        intent.putExtra("url", url);
        startActivityForResult(intent, REQ_CODE_LOGIN_SIGNUP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_LOGIN_SIGNUP) {
            // If resultCode was OK, login/signup was successful, now show menu
            if (resultCode == Activity.RESULT_OK) {
                mSuccess = true;
            }

            dismiss();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.v(TAG, "onDismiss called");

        if (mListener != null) {
            if (mSuccess) {
                mListener.onSuccess();
            } else {
                mListener.onCancelled();
            }
        }

        super.onDismiss(dialog);
    }
}
