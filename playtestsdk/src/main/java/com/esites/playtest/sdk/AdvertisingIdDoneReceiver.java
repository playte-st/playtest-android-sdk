package com.esites.playtest.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import javax.inject.Inject;

/**
 * Created by tnanlohy on 09/06/16.
 */
public class AdvertisingIdDoneReceiver extends BroadcastReceiver {

    @Inject
    PreferenceService mPreferenceService;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("DoneReceiver", "onReceive");

        Playtest.getAppComponent().inject(this);

        Playtest.startUp(context, mPreferenceService);
    }
}
