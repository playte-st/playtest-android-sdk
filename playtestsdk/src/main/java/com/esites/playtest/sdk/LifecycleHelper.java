package com.esites.playtest.sdk;

import android.app.Activity;
import android.app.Application;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by tnanlohy on 19/05/16.
 */
public class LifecycleHelper implements Application.ActivityLifecycleCallbacks {

    private static final String TAG = "LifecycleHelper";

    private AtomicInteger mCount = new AtomicInteger(0);

    private Set<Activity> mActivityList = new HashSet<>();

    private static LifecycleHelper sInstance;

    public static LifecycleHelper getInstance() {
        if (sInstance == null) {
            sInstance = new LifecycleHelper();
        }
        return sInstance;
    }

    /**************************************************************************************************
     * Lifecycle methods
     **************************************************************************************************/

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        mCount.incrementAndGet();
        Log.d(TAG, "onActivityStarted: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);

        mActivityList.add(activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        mCount.incrementAndGet();
        Log.d(TAG, "onActivityResumed: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);
        showActivityList();


        // When the count = 2, we come from the background
        if (mCount.get() == 2 && !fromSdkPackage(activity)) {
            Playtest.initBubble(activity);
        }

        if (Playtest.isBubbleNull()) {
            Playtest.reInitializeBubble(activity);
        }
        Log.i(TAG, "Check if bubble is null: " + Playtest.isBubbleNull());
    }

    @Override
    public void onActivityPaused(Activity activity) {
        mCount.decrementAndGet();
        Log.d(TAG, "onActivityPaused: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        mCount.decrementAndGet();
        Log.d(TAG, "onActivityStopped: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);

        mActivityList.remove(activity);

        // Whenever count is 0, we went to the background
        if (mCount.get() == 0) {
            Playtest.destroyBubble();
            mActivityList.clear();
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.d(TAG, "onActivityDestroyed: " + activity.getLocalClassName() + ", ===> mCount: " + mCount);
    }

    /**
     * Checks whether an Activity belongs to the Sdk package
     * @param activity
     * @return
     */
    private boolean fromSdkPackage(Activity activity) {
        String packageName = activity.getClass().getPackage().toString();
        return packageName.contains(BuildConfig.APPLICATION_ID);
    }

    /**************************************************************************************************
     * Helper methods
     **************************************************************************************************/

    /**
     *
     * Get the view that we need to take a screenshot of
     * @return
     */
    public View getView() {
        Activity activity = getTopActivity();
        if (activity == null) return null;

        View view = checkGLSurfaceView(activity);
        if (view != null && view instanceof GLSurfaceView) {
            return view;
        } else {
            return activity.getWindow().getDecorView().getRootView();
        }
    }

    private View checkGLSurfaceView(Activity activity) {
        ViewGroup content = (ViewGroup) activity.findViewById(android.R.id.content);
        int count = content.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = content.getChildAt(i);
            Log.w(TAG, "Child: " + i + ": " + view.getClass().getSimpleName());
            if (view instanceof ViewGroup) {
                ViewGroup childGroup = ((ViewGroup)view);
                int childCount = childGroup.getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View child = childGroup.getChildAt(j);
                    Log.w(TAG, "ViewGroup child: " + i + ": " + child.getClass().getSimpleName());
                    if (child instanceof GLSurfaceView) {
                        return child;
                    }
                }
            } else if (view instanceof GLSurfaceView) {
                return view;
            }
        }
        return null;
    }

    /**
     * Return the Activity that is on top of the Activity stack. This Activity list only contains Activities
     * that are NOT inside the SDK package
     *
     * @return
     */
    public Activity getTopActivity() {
        for (Activity activity : mActivityList) {
            if (!fromSdkPackage(activity)) {
                return activity;
            }
        }
        return null;
    }

    /**
     * Just for logging how many items we have in our mActivityList
     */
    public void showActivityList() {
        Log.i(TAG, "mActivityList.size(): " + mActivityList.size());
        int index = 0;
        for (Activity activity : mActivityList) {
            Log.i(TAG, "Activity [" + index + "]: " + activity.getLocalClassName());
            index++;
        }
    }

    /**
     * This will return true whenever the helper count = 2.
     * The counter will add and extract at all lifecycle methods. onStart and onResume will be called whenever
     * an app is first started so the count should be 2.
     *
     * @return
     */
    public boolean isAppResumed() {
        Log.v(TAG, "isAppResumed mCount: " + mCount);
        return mCount.get() == 2;
    }
}
