package com.esites.playtest.sdk.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tnanlohy on 07/06/16.
 */
public class User {

    @SerializedName("displayName")
    private String mDisplayName;

    @SerializedName("name")
    private String mName;

    @SerializedName("lastname")
    private String mLastName;

    @SerializedName("avatarURL")
    private String mAvatarUrl;

    @SerializedName("isAnonymous")
    private boolean mIsAnonymous;

    @SerializedName("sessionID")
    private String mSessionId;

    @SerializedName("playtestID")
    private String mPlaytestId;

    @SerializedName("apiToken")
    private String mUserApiToken;

    public User() {
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }

    public void setAnonymous(boolean anonymous) {
        mIsAnonymous = anonymous;
    }

    public void setSessionId(String sessionId) {
        mSessionId = sessionId;
    }

    public void setPlaytestId(String playtestId) {
        mPlaytestId = playtestId;
    }

    public void setUserApiToken(String userApiToken) {
        mUserApiToken = userApiToken;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public String getName() {
        return mName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public boolean isAnonymous() {
        return mIsAnonymous;
    }

    public String getSessionId() {
        return mSessionId;
    }

    public String getPlaytestId() {
        return mPlaytestId;
    }

    public String getUserApiToken() {
        return mUserApiToken;
    }
}
