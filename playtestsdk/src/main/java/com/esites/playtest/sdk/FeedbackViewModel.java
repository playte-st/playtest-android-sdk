package com.esites.playtest.sdk;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.esites.playtest.sdk.model.Feedback;
import com.esites.playtest.sdk.model.RealmFeedback;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackViewModel extends BaseObservable {

    private final Feedback mFeedback;

    private final SimpleDateFormat mSimpleDateFormat;

    public FeedbackViewModel(Feedback feedback) {
        mFeedback = feedback;
        mSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
    }

    public String getUserDisplayName() {
        return mFeedback.getUserDisplayName();
    }

    public String getDateCreated() {
        if (mFeedback.getDateUpdated() != null) {
            return mSimpleDateFormat.format(mFeedback.getDateUpdated());
        } else if (mFeedback.getDateCreated() != null) {
            return mSimpleDateFormat.format(mFeedback.getDateCreated());
        }

        return "";
    }

    public String getFeedback() {
        return mFeedback.getFeedback();
    }

    public String getAvatarUrl() {
        return mFeedback.getAvatarUrl();
    }

    public String getThumbUrl() {
        return mFeedback.getThumbUrl();
    }

    public int getThumbWidth() {
        return mFeedback.getThumbWidth();
    }

    public int getThumbHeight() {
        return mFeedback.getThumbHeight();
    }

    public String getLikeCount() {
        return String.valueOf(mFeedback.getVoteCount());
    }

    public boolean getIsLiked() {
        return mFeedback.isLiked();
    }

    @BindingAdapter({"bind:avatarUrl"})
    public static void loadAvatar(ImageView imageView, String avatarUrl) {
        DisplayUtils.loadWithGlideCenterCropped(imageView, avatarUrl);
    }

    @BindingAdapter({"bind:thumbUrl"})
    public static void loadThumb(ImageView imageView, String thumbUrl) {
        DisplayUtils.loadWithGlideCenterCropped(imageView, thumbUrl);
    }

    @BindingAdapter({"bind:likeDrawable"})
    public static void setLikeDrawable(TextView textView, boolean isLiked) {
        int likedResourceId = isLiked ? R.drawable.ic_like_blue : R.drawable.ic_like_grey;
        textView.setCompoundDrawablesWithIntrinsicBounds(0, likedResourceId, 0, 0);
    }

    @BindingAdapter({"bind:thumbWidth", "bind:thumbHeight"})
    public static void resize(ImageView imageView, int thumbWidth, int thumbHeight) {
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();

        // Doubling the size of the thumb
        Context context = imageView.getContext();
        layoutParams.width = thumbWidth * 2;
        layoutParams.height = thumbHeight * 2;
        imageView.setLayoutParams(layoutParams);
    }
}
