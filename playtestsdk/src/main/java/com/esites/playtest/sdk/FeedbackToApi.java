package com.esites.playtest.sdk;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tnanlohy on 17/05/16.
 */
public class FeedbackToApi {

    @SerializedName("playtestSession")
    private String mSessionId;

    @SerializedName("feedback")
    private String mFeedback;

    public FeedbackToApi(String sessionId, String feedback) {
        mSessionId = sessionId;
        mFeedback = feedback;
    }
}
