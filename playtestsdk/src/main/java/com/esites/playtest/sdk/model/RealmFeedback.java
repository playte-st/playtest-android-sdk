package com.esites.playtest.sdk.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by tnanlohy on 25/05/16.
 */
public class RealmFeedback extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private String id;

    @SerializedName("feedback")
    private String feedback;

    @SerializedName("liked")
    private boolean liked;

    @SerializedName("owner")
    private boolean owner;

    @SerializedName("seen")
    private boolean seen;

    @SerializedName("imageURL")
    private String imageUrl;

    @SerializedName("imageWidth")
    private int imageWidth;

    @SerializedName("imageHeight")
    private int imageHeight;

    @SerializedName("imageDevice")
    private String imageDevice;

    @SerializedName("thumbURL")
    private String thumbUrl;

    @SerializedName("thumbWidth")
    private int thumbWidth;

    @SerializedName("thumbHeight")
    private int thumbHeight;

    @SerializedName("metaData")
    private MetaData metaData;

    @SerializedName("eventData")
    private EventData eventData;

    @SerializedName("playtest")
    private String playtestId;

    @SerializedName("session")
    private String sessionId;

    @SerializedName("createdAt")
    private Date dateCreated;

    @SerializedName("updatedAt")
    private Date dateUpdated;

    @SerializedName("voteCount")
    private int voteCount;

    @SerializedName("userID")
    private String userId;

    @SerializedName("name")
    private String userName;

    @SerializedName("lastname")
    private String userLastName;

    @SerializedName("displayName")
    private String userDisplayName;

    @SerializedName("avatarUrl")
    private String avatarUrl;

    @SerializedName("isStudio")
    private boolean isStudio;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getImageDevice() {
        return imageDevice;
    }

    public void setImageDevice(String imageDevice) {
        this.imageDevice = imageDevice;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public EventData getEventData() {
        return eventData;
    }

    public void setEventData(EventData eventData) {
        this.eventData = eventData;
    }

    public String getPlaytestId() {
        return playtestId;
    }

    public void setPlaytestId(String playtestId) {
        this.playtestId = playtestId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        this.userDisplayName = userDisplayName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public boolean isStudio() {
        return isStudio;
    }

    public void setStudio(boolean studio) {
        isStudio = studio;
    }
}
