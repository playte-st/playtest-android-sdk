package com.esites.playtest.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

/**
 * Created by tnanlohy on 25/05/16.
 */
public class DisplayUtils {

    /**
     * Create and add a Fragment to the content layout
     * @param cls The Fragment class
     * @param args Arguments for the Fragment
     * @param tag The tag for our Fragment instance
     * @param addToBackStack Do we need to add this Fragment to the backstack?
     */
    public static void createAndReplaceFragment(@IdRes int contentResId, FragmentManager fragmentManager,
                                                Class<? extends Fragment> cls, Bundle args, String tag,
                                                boolean addToBackStack) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        if(fragment == null) {

            try {
                fragment = cls.newInstance();
                if (args != null) {
                    fragment.setArguments(args);
                }


            } catch (InstantiationException e) {
                Log.e("DisplayUtils", "InstantiationException: ", e);
            } catch (IllegalAccessException e) {
                Log.e("DisplayUtils", "IllegalAccessException: ", e);
            }
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        if(addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
//        }

        fragmentTransaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom);

        fragmentTransaction.add(contentResId, fragment, tag);
        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();
    }

    public static void removeFragment(FragmentManager fragmentManager, String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit();
        }
    }

    /**
     * Load an image url with Glide, and set scaleType to centerCrop
     * @param view
     * @param url
     */
    public static void loadWithGlideCenterCropped(ImageView view, String url) {
        Glide.with(view.getContext())
             .load(url)
             .asBitmap()
             .centerCrop()
             .into(view);
    }

    public static Intent createScreenshotIntent(Context context, String filePath) {
        Intent intent = new Intent(context, ScreenshotActivity.class);
        intent.putExtra("file_path", filePath);
        return intent;
    }

    public static Intent createFeedbackWithScreenshotIntent(Context context, String filePath) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra("file_path", filePath);
        return intent;
    }

    public static boolean deleteFile(File currentFile) {
        return currentFile.delete();
    }
}
