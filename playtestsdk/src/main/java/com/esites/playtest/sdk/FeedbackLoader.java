package com.esites.playtest.sdk;

import android.util.Log;

import com.esites.playtest.sdk.model.RealmFeedback;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackLoader {

    private static final String TAG = "FeedbackLoader";
    private final ApiService mApiService;
    private final Realm      mRealm;
    private final PreferenceService      mPreferenceService;

    private FeedbackLoaderListener mListener;

    public interface FeedbackLoaderListener {
        void onFeedbackLoaded();

        void onErrorLoadingFeedback();
    }

    public FeedbackLoader(ApiService apiService, Realm realm, PreferenceService preferenceService) {
        mApiService = apiService;
        mRealm = realm;
        mPreferenceService = preferenceService;
    }

    public void setListener(FeedbackLoaderListener listener) {
        mListener = listener;
    }

    public void loadFeedback() {
        //TODO: Add real playTest session id
        String playtestId = mPreferenceService.getPlaytestId();
        if (playtestId != null) {
            mApiService.getFeedback(playtestId).enqueue(new Callback<RealmList<RealmFeedback>>() {
                @Override
                public void onResponse(Call<RealmList<RealmFeedback>> call, Response<RealmList<RealmFeedback>> response) {
                    if (response.isSuccessful()) {
                        onFeedbackLoaded(response.body());
                    } else {
                        callError();
                    }
                }

                @Override
                public void onFailure(Call<RealmList<RealmFeedback>> call, Throwable t) {
                    Log.e(TAG, "======== onFailure: ", t);
                    callError();
                }
            });
        } else {
            callError();
        }


    }

    private void callError() {
        if (mListener != null) {
            mListener.onErrorLoadingFeedback();
        }
    }

    private void onFeedbackLoaded(RealmList<RealmFeedback> feedbackList) {
        Log.wtf(TAG, "======== onFeedback loaded! with size: " + feedbackList.size());

        // Add all new
        //TODO: Updating does not delete items from Realm


        RealmResults<RealmFeedback> current = mRealm.where(RealmFeedback.class).findAll();
        mRealm.beginTransaction();
        current.deleteAllFromRealm();
        mRealm.commitTransaction();

        mRealm.beginTransaction();
        mRealm.copyToRealm(feedbackList);
//        mRealm.copyToRealmOrUpdate(feedbackList);
        mRealm.commitTransaction();

        if (mListener != null) {
            mListener.onFeedbackLoaded();
        }
    }
}
