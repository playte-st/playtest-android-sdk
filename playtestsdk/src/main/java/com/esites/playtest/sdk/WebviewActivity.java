package com.esites.playtest.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.esites.playtest.sdk.model.User;

import javax.inject.Inject;

/**
 * Created by tnanlohy on 07/06/16.
 */
public class WebviewActivity extends AppCompatActivity {

    @Inject
    PreferenceService mPreferenceService;

    private static final String TAG = "WebviewActivity";

    private WebView mWebView;
    private String  mUrl;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Playtest.getAppComponent().inject(this);

        mUrl = getIntent().getStringExtra("url");

        setContentView(R.layout.activity_webview);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);

        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("glahquq0:")) {
                    handleSuccessfulLoginOrSignup(url);
                    return false;
                } else {
                    view.loadUrl(url);
                    return true;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
            }
        });
        mWebView.getSettings().setJavaScriptEnabled(true);

        load();
    }

    private void handleSuccessfulLoginOrSignup(String url) {
        Uri uri = Uri.parse(url);
        User user = mPreferenceService.user().get();
        if (user == null) {
            user = new User();
        }
        user.setDisplayName(uri.getQueryParameter("displayName"));
        user.setName(uri.getQueryParameter("name"));
        user.setLastName(uri.getQueryParameter("lastname"));
        user.setAvatarUrl(uri.getQueryParameter("avatarURL"));
        user.setUserApiToken(uri.getQueryParameter("apiToken"));
        user.setAnonymous(false);

        Log.v(TAG, "user.displayName: " + user.getDisplayName() + ", name: " + user.getName());

        // Set new user
        mPreferenceService.user().set(user);

        setResult(RESULT_OK);
        finish();
    }

    private void load() {
        if (isInternetAvailable()) {
            mWebView.loadUrl(mUrl);
        } else {
            Snackbar.make(mWebView, R.string.error_no_network, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.error_action_retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            load();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }
}
