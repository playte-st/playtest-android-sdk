package com.esites.playtest.sdk;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Dagger module for all data classes
 */
@Module(includes = {ApiModule.class, PersistenceModule.class})
class DataModule {

    @Singleton
    @Provides
    FeedbackLoader provideFeedbackLoader(ApiService apiService, Realm realm, PreferenceService preferenceService) {
        return new FeedbackLoader(apiService, realm, preferenceService);
    }
}
