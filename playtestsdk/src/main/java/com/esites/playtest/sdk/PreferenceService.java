package com.esites.playtest.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.esites.playtest.sdk.model.User;
import com.f2prateek.rx.preferences.Preference;
import com.f2prateek.rx.preferences.RxSharedPreferences;
import com.google.gson.Gson;

/**
 * Class that stores User related data. A better and easier working SharedPreferences class
 */
public class PreferenceService {

    private static final String KEY_STUDIO_API_TOKEN = "studio_api_token";
    private static final String KEY_USER_API_TOKEN = "user_api_token";
    private static final String KEY_PLAYTEST_GUID = "playtest_guid";
    private static final String KEY_DEVICE_UUID = "device_uuid";
    private static final String KEY_NEEDS_VALIDATION = "needs_validation";

    private final Preference<String>  mStudioApiToken;
    private final Preference<String>  mPlaytestGuid;
    private final Preference<String>  mDeviceUUID;
    private final Preference<User>    mUser;
    private final Preference<Boolean> mNeedsValidation;

    public PreferenceService(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        RxSharedPreferences rxPreferences = RxSharedPreferences.create(preferences);

        mStudioApiToken = rxPreferences.getString(KEY_STUDIO_API_TOKEN, null);
        mPlaytestGuid = rxPreferences.getString(KEY_PLAYTEST_GUID, null);
        mDeviceUUID = rxPreferences.getString(KEY_DEVICE_UUID, null);
        mNeedsValidation = rxPreferences.getBoolean(KEY_NEEDS_VALIDATION, false);

        GsonPreferenceAdapter<User> userAdapter = new GsonPreferenceAdapter<>(new Gson(), User.class);
        mUser = rxPreferences.getObject(KEY_USER_API_TOKEN, userAdapter);

    }

    @Nullable
    public String getStudioApiToken() {
        return mStudioApiToken.get();
    }

    @Nullable
    public String getPlaytestGuid() {
        return mPlaytestGuid.get();
    }

    @Nullable
    public String getUserApiToken() {
        User user = mUser.get();
        if (user != null) {
            return user.getUserApiToken();
        }
        return null;
    }

    @Nullable
    public String getPlaytestId() {
        User user = mUser.get();
        if (user != null) {
            return user.getPlaytestId();
        }
        return null;
    }

    @Nullable
    public String getSessionId() {
        User user = mUser.get();
        if (user != null) {
            return user.getSessionId();
        }
        return null;
    }

    @Nullable
    public String getDeviceUuid() {
        return mDeviceUUID.get();
    }

    public Preference<String> deviceUuid() {
        return mDeviceUUID;
    }

    /**
     * Use this to set the studio api token
     * @return
     */
    public Preference<String> studioApiToken() {
        return mStudioApiToken;
    }

    /**
     * Use this to save the user
     * @return
     */
    public Preference<User> user() {
        return mUser;
    }

    /**
     * Use this to set the playtest guid
     * @return
     */
    public Preference<String> playtestGuid() {
        return mPlaytestGuid;
    }

    public Preference<Boolean> needsValidation() {
        return mNeedsValidation;
    }

    public boolean getNeedsValidation() {
        Boolean needsValidation = mNeedsValidation.get();
        if (needsValidation == null) {
            return false;
        }
        return needsValidation;
    }
}
