package com.esites.playtest.sdk.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tnanlohy on 13/06/16.
 */
public class HeartbeatResponse {

    @SerializedName("unseenFeedbacks")
    private int mUnseenFeedbacks;

    public int getUnseenFeedbacks() {
        return mUnseenFeedbacks;
    }
}
