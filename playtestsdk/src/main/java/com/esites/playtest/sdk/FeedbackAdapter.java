package com.esites.playtest.sdk;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esites.playtest.sdk.model.Feedback;
import com.esites.playtest.sdk.model.RealmFeedback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackAdapter extends RecyclerView.Adapter {

    private static final String TAG = "FeedbackAdapter";
    private final LayoutInflater mLayoutInflater;
    private       List<Feedback> mFeedbackList = new ArrayList<>();
    private FeedbackViewHolder.ItemListener mItemListener;

    public FeedbackAdapter(Context context, List<Feedback> feedbackList, FeedbackViewHolder.ItemListener itemListener) {
        mLayoutInflater = LayoutInflater.from(context);
        mItemListener = itemListener;
        setFeedback(feedbackList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_feedback, parent, false);
        return new FeedbackViewHolder(view, mItemListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Feedback feedback = mFeedbackList.get(position);
        ((FeedbackViewHolder) holder).bind(feedback);
    }

    @Override
    public int getItemCount() {
        return mFeedbackList.size();
    }

    public void setFeedback(List<Feedback> feedbackList) {
        Log.i(TAG, "feedbackList: " + feedbackList.size());
        mFeedbackList.clear();
        mFeedbackList.addAll(feedbackList);
        notifyDataSetChanged();
    }

    public void addFeedback(Feedback feedback) {
        int position = mFeedbackList.indexOf(feedback);
        mFeedbackList.add(feedback);
        notifyItemChanged(position);
    }

    public void removeFeedback(Feedback feedback) {
        int position = mFeedbackList.indexOf(feedback);
        mFeedbackList.remove(feedback);
        notifyItemRemoved(position);
    }

    public void changeFeedback(Feedback newFeedback) {
        int position = -1;
        for (int i = 0; i < mFeedbackList.size(); i++) {
            Feedback feedback = mFeedbackList.get(i);
            if (feedback.getId().equals(newFeedback.getId())) {
                position = i;
            }
        }

        // Change item if it was present in the list
        if (position != -1) {
            Log.w(TAG, "Notifying item changed liked: " + newFeedback.isLiked());
            mFeedbackList.set(position, newFeedback);
            notifyItemChanged(position);
        }
    }
}
