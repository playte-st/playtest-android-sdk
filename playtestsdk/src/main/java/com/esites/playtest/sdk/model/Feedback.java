package com.esites.playtest.sdk.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by tnanlohy on 01/06/16.
 */
public class Feedback {

    @SerializedName("id")
    private String id;

    @SerializedName("feedback")
    private String feedback;

    @SerializedName("liked")
    private boolean liked;

    @SerializedName("owner")
    private boolean owner;

    @SerializedName("seen")
    private boolean seen;

    @SerializedName("imageURL")
    private String imageUrl;

    @SerializedName("imageWidth")
    private int imageWidth;

    @SerializedName("imageHeight")
    private int imageHeight;

    @SerializedName("imageDevice")
    private String imageDevice;

    @SerializedName("thumbURL")
    private String thumbUrl;

    @SerializedName("thumbWidth")
    private int thumbWidth;

    @SerializedName("thumbHeight")
    private int thumbHeight;

    @SerializedName("metaData")
    private MetaData metaData;

    @SerializedName("eventData")
    private EventData eventData;

    @SerializedName("playtest")
    private String playtestId;

    @SerializedName("session")
    private String sessionId;

    @SerializedName("createdAt")
    private Date dateCreated;

    @SerializedName("updatedAt")
    private Date dateUpdated;

    @SerializedName("voteCount")
    private int voteCount;

    @SerializedName("userID")
    private String userId;

    @SerializedName("name")
    private String userName;

    @SerializedName("lastname")
    private String userLastName;

    @SerializedName("displayName")
    private String userDisplayName;

    @SerializedName("avatarUrl")
    private String avatarUrl;

    @SerializedName("isStudio")
    private boolean isStudio;

    public Feedback(RealmFeedback realmFeedback) {
        id = realmFeedback.getId();
        feedback = realmFeedback.getFeedback();
        liked = realmFeedback.isLiked();
        owner = realmFeedback.isOwner();
        seen = realmFeedback.isSeen();
        imageUrl = realmFeedback.getImageUrl();
        imageWidth = realmFeedback.getImageWidth();
        imageHeight = realmFeedback.getImageHeight();
        imageDevice = realmFeedback.getImageDevice();
        thumbUrl = realmFeedback.getThumbUrl();
        thumbWidth = realmFeedback.getThumbWidth();
        thumbHeight = realmFeedback.getThumbHeight();
        metaData = realmFeedback.getMetaData();
        eventData = realmFeedback.getEventData();
        playtestId = realmFeedback.getPlaytestId();
        sessionId = realmFeedback.getSessionId();
        dateCreated = realmFeedback.getDateCreated();
        dateUpdated = realmFeedback.getDateUpdated();
        voteCount = realmFeedback.getVoteCount();
        userId = realmFeedback.getUserId();
        userName = realmFeedback.getUserName();
        userLastName = realmFeedback.getUserLastName();
        userDisplayName = realmFeedback.getUserDisplayName();
        avatarUrl = realmFeedback.getAvatarUrl();
        isStudio = realmFeedback.isStudio();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        feedback = feedback;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        liked = liked;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        owner = owner;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        seen = seen;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        imageUrl = imageUrl;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        imageHeight = imageHeight;
    }

    public String getImageDevice() {
        return imageDevice;
    }

    public void setImageDevice(String imageDevice) {
        imageDevice = imageDevice;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        thumbUrl = thumbUrl;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        thumbWidth = thumbWidth;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        thumbHeight = thumbHeight;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        metaData = metaData;
    }

    public EventData getEventData() {
        return eventData;
    }

    public void setEventData(EventData eventData) {
        eventData = eventData;
    }

    public String getPlaytestId() {
        return playtestId;
    }

    public void setPlaytestId(String playtestId) {
        playtestId = playtestId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        sessionId = sessionId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        dateUpdated = dateUpdated;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        voteCount = voteCount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        userName = userName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        userLastName = userLastName;
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public void setUserDisplayName(String userDisplayName) {
        userDisplayName = userDisplayName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        avatarUrl = avatarUrl;
    }

    public boolean isStudio() {
        return isStudio;
    }

    public void setStudio(boolean studio) {
        isStudio = studio;
    }
}
