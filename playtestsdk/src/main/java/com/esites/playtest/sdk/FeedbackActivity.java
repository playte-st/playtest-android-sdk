package com.esites.playtest.sdk;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.esites.playtest.sdk.model.Feedback;
import com.esites.playtest.sdk.model.RealmFeedback;
import com.esites.playtest.sdk.widget.CustomRecyclerView;
import com.esites.playtest.sdk.widget.MultiStateView;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackActivity extends AppCompatActivity implements FeedbackLoader.FeedbackLoaderListener,
        View.OnClickListener, FeedbackViewHolder.ItemListener {

    private static final String TAG              = "FeedbackActivity";
    private static final int REQ_CODE_SCREENSHOT = 999;
    @Inject
    FeedbackLoader mFeedbackLoader;

    @Inject
    Realm mRealm;

    @Inject
    ApiService mApiService;

    @Inject
    PreferenceService mPreferenceService;

    private CustomRecyclerView          mRecyclerView;
    private LinearLayoutManager         mLayoutManager;
    private MultiStateView              mStateView;
    private RealmResults<RealmFeedback> mFeedbackList;

    private FeedbackAdapter mAdapter;
    private EditText        mFeedbackEdit;
    private View            mSendButton;

    private File mCurrentScreenshotFile;
    private boolean mHasScreenshot = false;

    private FeedbackCallback mFeedbackCallback = new FeedbackCallback();

    private ProgressBar mProgressBar;
    private View        mScreenshotButton;
    private String      mFilePath;
    private View        mCommentLayout;
    private View        mScreenshotLayout;
    private ImageView   mScreenshotView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        Playtest.getAppComponent().inject(this);

        setTitle("Feedback");

        mFilePath = getIntent().getStringExtra("file_path");
        if (mFilePath != null) {
            mCurrentScreenshotFile = new File(mFilePath);
            mHasScreenshot = mCurrentScreenshotFile.exists();
            Log.wtf(TAG, "Screenshot file exists: " + mCurrentScreenshotFile.exists());
        }

        mStateView = (MultiStateView) findViewById(R.id.state_view);


        // Get database list of feedback and send these to the adapter. Be sure to set this block of BEFORE
        // actually setting the adapter
        mFeedbackList = mRealm.where(RealmFeedback.class).findAllSorted("dateUpdated", Sort.ASCENDING);
        List<Feedback> list = convertRealmResultsToList();
        mAdapter = new FeedbackAdapter(this, list, this);


        // Now load feedback from the api
        mFeedbackLoader.setListener(this);
        mFeedbackLoader.loadFeedback();


        mRecyclerView = (CustomRecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setDataListener(new CustomRecyclerView.DataListener() {
            @Override
            public void onDataPresent() {
                mStateView.setViewState(MultiStateView.ViewState.CONTENT);
            }

            @Override
            public void onDataMissing() {
                mStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }

            @Override
            public void onDataLoading() {
                mStateView.setViewState(MultiStateView.ViewState.LOADING);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mFeedbackEdit = (EditText) findViewById(R.id.feedback_edit);
        mFeedbackEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                toggleSendButtonEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSendButton = findViewById(R.id.send_button);
        mSendButton.setOnClickListener(this);
        toggleSendButtonEnabled();

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);

        mScreenshotButton = findViewById(R.id.show_screenshot);
        mScreenshotButton.setOnClickListener(this);

        // Make sure the comment layout is brought to the front
        mCommentLayout = findViewById(R.id.comment_layout);
        mCommentLayout.bringToFront();

        // Show screenshot if needed
        mScreenshotLayout = findViewById(R.id.screenshot_layout);
        mScreenshotView = (ImageView) findViewById(R.id.screenshot);
        mScreenshotView.setOnClickListener(this);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        RectF drawableRect = new RectF(0, 0, widthPixels, heightPixels);
        RectF viewRect = new RectF(0, 0, widthPixels, heightPixels);
        Matrix matrix = new Matrix();
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.START);
        mScreenshotView.setImageMatrix(matrix);

        findViewById(R.id.hide_screenshot).setOnClickListener(this);

        checkNeedsScreenshot();
    }

    private void checkNeedsScreenshot() {
        if (mHasScreenshot) {
            // If we do have a screenshot, show it inside its view
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentScreenshotFile.getAbsolutePath());
            mScreenshotView.setImageBitmap(bitmap);
        } else {
            hideScreenshotView(0, false);
        }
    }

    private void showScreenshotView() {
        Log.v(TAG, "ShowScreenshot view");
        int toY = getResources().getDimensionPixelSize(R.dimen.move_up_80_dp);
        mScreenshotLayout.animate()
                      .translationY(toY)
                      .setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mScreenshotButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mStateView.getLayoutParams();
        int bottom = getResources().getDimensionPixelSize(R.dimen.move_down_80_dp);
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottom);

        mScreenshotButton.setVisibility(View.GONE);
    }

    private void hideScreenshotView(long duration, final boolean setToVisible) {
        Log.v(TAG, "Hide screenshot view");
        int toY = getResources().getDimensionPixelSize(R.dimen.move_down_80_dp);
        mScreenshotLayout.animate()
                         .translationY(toY)
                         .setDuration(duration).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (setToVisible) {
                    mScreenshotButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mStateView.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, 0);

    }

    private List<Feedback> convertRealmResultsToList() {
        List<Feedback> list = new ArrayList<>();
        for (RealmFeedback realmFeedback : mFeedbackList) {
            Feedback feedback = new Feedback(realmFeedback);
            list.add(feedback);
        }
        return list;
    }

    private void toggleSendButtonEnabled() {
        boolean isEmpty = mFeedbackEdit.getText().toString().isEmpty();
        mSendButton.setEnabled(!isEmpty);
    }

    @Override
    public void onFeedbackLoaded() {
        Log.i(TAG, "onFeedbackLoaded size feedbackList: " + mFeedbackList.size());
        loadFeedback();
    }

    @Override
    public void onErrorLoadingFeedback() {
        Log.i(TAG, "onErrorLoadingFeedback");
        mStateView.setViewState(MultiStateView.ViewState.ERROR);
    }

    private void loadFeedback() {
        mFeedbackList = mRealm.where(RealmFeedback.class).findAllSorted("dateUpdated", Sort.ASCENDING);

        // Calculate if there was any difference in feedback count
        int sizeAdapter = mAdapter.getItemCount();
        int sizeAfterDownload = mFeedbackList.size();

        if (sizeAfterDownload != sizeAdapter) {
            List<Feedback> list = convertRealmResultsToList();
            mAdapter.setFeedback(list);
        }

        mLayoutManager.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.send_button) {
            sendFeedback();
        } else if (id == R.id.show_screenshot) {
            showScreenshotView();
        } else if (id == R.id.hide_screenshot) {
            hideScreenshotView(500, true);
        } else if (id == R.id.screenshot) {
            showScreenshot();
        }
    }

    private void showScreenshot() {
        Intent intent = DisplayUtils.createScreenshotIntent(this, mFilePath);
        startActivityForResult(intent, REQ_CODE_SCREENSHOT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Whenever the user cancels the screenshot, we can finish Activities until the menu shows
        if (requestCode == REQ_CODE_SCREENSHOT && resultCode == RESULT_OK) {
            deleteScreenshot();
        }

        finish();
    }

    private void toggleCommentView(boolean enabled) {
        mProgressBar.setVisibility(enabled ? View.GONE : View.VISIBLE);
        mFeedbackEdit.setEnabled(enabled);
        mSendButton.setEnabled(enabled);
    }

    private void sendFeedback() {
        // Toggle comment view
        toggleCommentView(false);

        String feedbackText = mFeedbackEdit.getText().toString();
        String sessionId = mPreferenceService.getSessionId();
        if (sessionId != null) {
            RequestBody sessionBody = RequestBody.create(MediaType.parse("multipart/form-data"), sessionId);
            RequestBody feedbackBody = RequestBody.create(MediaType.parse("multipart/form-data"), feedbackText);

            if (mHasScreenshot) {
                RequestBody screenshotBody = RequestBody.create(MediaType.parse("image/*"), mCurrentScreenshotFile);
                mFeedbackCallback.sendFeedback(sessionBody, feedbackBody, screenshotBody);
            } else {
                mFeedbackCallback.sendFeedbackNoScreenshot(sessionBody, feedbackBody);
            }

        } else {
            Toast.makeText(this, "We have no active Playtest session, cannot post feedback", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLikedClicked(String feedbackId) {
        mApiService.like(feedbackId).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    handleLikeSucces(response.body());
                } else {
                    handleLikeError(response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                handleLikeError(t.getMessage());
            }
        });
    }

    private void handleLikeSucces(JsonElement body) {
        Feedback feedback = handleFeedbackBody(body);
        Log.w(TAG, "handling success: " + feedback);
        mAdapter.changeFeedback(feedback);

    }

    /**
     * Handles the feedback api call
     */
    class FeedbackCallback implements Callback<JsonElement> {

        public void sendFeedback(RequestBody sessionBody, RequestBody feedbackBody, RequestBody screenshot) {
            mApiService.postFeedback(screenshot, sessionBody, feedbackBody).enqueue(this);
        }

        public void sendFeedbackNoScreenshot(RequestBody sessionBody, RequestBody feedbackBody) {
            mApiService.postFeedbackNoScreenshot(sessionBody, feedbackBody).enqueue(this);
        }

        @Override
        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
            if (response.isSuccessful()) {
                handleSuccess(response.body());
            } else {
                handleError(response.message());
            }
        }

        @Override
        public void onFailure(Call<JsonElement> call, Throwable t) {
            Log.e(TAG, "onFailure sending feedback", t);
            handleError(t.getMessage());
        }
    }

    private void handleSuccess(JsonElement body) {

        Feedback feedback = handleFeedbackBody(body);

        // Set view to enabled
        toggleCommentView(true);

        // Empty the feedback edit text
        mFeedbackEdit.setText("");

        // Delete the file
        deleteScreenshot();

        mAdapter.addFeedback(feedback);
        mLayoutManager.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void deleteScreenshot() {
        Log.v(TAG, "Delete screenshot");
        if (mHasScreenshot && mCurrentScreenshotFile != null) {
            if (mCurrentScreenshotFile.exists()) {
                DisplayUtils.deleteFile(mCurrentScreenshotFile);
            }
        }

        // Set instance properties back to default
        hideScreenshotView(500, false);
        mScreenshotView.setImageResource(android.R.color.transparent);
        mCurrentScreenshotFile = null;
        mHasScreenshot = false;
        mScreenshotButton.setVisibility(View.GONE);

    }

    private Feedback handleFeedbackBody(JsonElement body) {
        Gson gson = new Gson();
        JsonObject feedbackObject = body.getAsJsonObject().get("feedback").getAsJsonObject();
        RealmFeedback realmFeedback = gson.fromJson(feedbackObject, RealmFeedback.class);

        mRealm.beginTransaction();
        mRealm.copyToRealmOrUpdate(realmFeedback);
        mRealm.commitTransaction();

        return new Feedback(realmFeedback);
    }

    private void handleError(String message) {
        // Enable comment view
        toggleCommentView(true);

        Log.e(TAG, "Error sending feedback: " + message);
        Toast.makeText(this, R.string.send_feedback_error, Toast.LENGTH_SHORT).show();
    }

    private void handleLikeError(String message) {
        Log.e(TAG, "Error liking feedback: " + message);
        Toast.makeText(this, R.string.like_error, Toast.LENGTH_SHORT).show();
    }
}
