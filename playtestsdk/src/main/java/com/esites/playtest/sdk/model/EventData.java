package com.esites.playtest.sdk.model;

import io.realm.RealmObject;

/**
 * Created by tnanlohy on 25/05/16.
 */
public class EventData extends RealmObject {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
