package com.esites.playtest.sdk;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.esites.playtest.sdk.databinding.ItemFeedbackBinding;
import com.esites.playtest.sdk.model.Feedback;
import com.esites.playtest.sdk.model.RealmFeedback;


/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final ItemFeedbackBinding mBinding;
    private       Feedback            mFeedback;
    private ItemListener mListener;



    public interface ItemListener {
        void onLikedClicked(String feedbackId);
    }

    public FeedbackViewHolder(final View itemView, ItemListener listener) {
        super(itemView);

        mBinding = DataBindingUtil.bind(itemView);

        mListener = listener;

        ImageView imageView = (ImageView) itemView.findViewById(R.id.thumbnail);
        itemView.findViewById(R.id.like_count).setOnClickListener(this);

        if (imageView == null) return;
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(itemView.getContext(), ImageDetailActivity.class);
                intent.putExtra("image_url", mFeedback.getImageUrl());
                itemView.getContext().startActivity(intent);
            }
        });
    }

    public void bind(Feedback feedback) {
        mFeedback = feedback;
        mBinding.setFeedback(new FeedbackViewModel(feedback));
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onLikedClicked(mFeedback.getId());
        }
    }
}
