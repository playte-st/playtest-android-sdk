package com.esites.playtest.sdk;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by tnanlohy on 24/05/16.
 */
public class ScreenshotActivity extends AppCompatActivity {

    private static final String TAG = "Screenshot";
    private AppBarLayout mAppBarLayout;
    private File mCurrentFile;
    private Bitmap mBitmap;

    private ImageView    mImageView;
    private DrawingView mDrawingView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screenshot);

        getWindow().getDecorView()
                   .setSystemUiVisibility(
                           View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                   View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                   View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                   View.SYSTEM_UI_FLAG_FULLSCREEN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.translucent_toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.screenshot_title);

        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar_layout);

        mImageView = (ImageView) findViewById(R.id.screenshot_image);
        mDrawingView = (DrawingView) findViewById(R.id.drawing_view);
        mDrawingView.setDrawingViewTouchListener(new DrawingView.DrawingViewTouchListener() {
            @Override
            public void onActionDown() {
                mAppBarLayout.setExpanded(false);
            }

            @Override
            public void onActionUp() {
                mAppBarLayout.setExpanded(true);
            }
        });

        setupImage();
    }

    private void setupImage() {
        String filePath = getIntent().getStringExtra("file_path");
        mCurrentFile = new File(filePath);

        if (mCurrentFile.exists()) {
            mBitmap = BitmapFactory.decodeFile(mCurrentFile.getAbsolutePath());
            mImageView.setImageBitmap(mBitmap);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_screenshot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save_screenshot) {
            saveScreenshot();
        } else if (id == R.id.cancel_screenshot) {
            cancel();
        }
        return true;
    }

    /**
     * Save screenshot and start activity with all feedback
     */
    private void saveScreenshot() {

        Bitmap overlay = Bitmap.createBitmap(mBitmap.getWidth(), mBitmap.getHeight(), mBitmap.getConfig());
        Canvas canvas = new Canvas(overlay);
        canvas.drawBitmap(mBitmap, new Matrix(), null);
        mDrawingView.draw(canvas);

        try {

            // Overwrite current file with new Bitmap
            FileOutputStream outputStream = new FileOutputStream(mCurrentFile);
            int quality = 100;
            overlay.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            Log.e(TAG, "Something went wrong saving the new screenshot");
        } finally {
            // Eventually start the FeedbackActivity
            startFeedbackActivity();
        }
    }

    private void startFeedbackActivity() {
        String filePath = getIntent().getStringExtra("file_path");
        Intent intent = DisplayUtils.createFeedbackWithScreenshotIntent(this, filePath);
        startActivity(intent);
        finish();
    }

    private void deleteImage() {
        boolean deleted = DisplayUtils.deleteFile(mCurrentFile);
        Log.i(TAG, "Image deleted: " + deleted);
    }

    @Override
    public void onBackPressed() {
        cancel();
    }

    void cancel() {
        deleteImage();
        setResult(RESULT_OK);
        finish();
    }
}
