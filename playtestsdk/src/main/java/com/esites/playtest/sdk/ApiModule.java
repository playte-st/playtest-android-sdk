package com.esites.playtest.sdk;

import android.content.Context;
import android.util.Log;
import android.webkit.WebSettings;

import com.esites.playtest.sdk.model.deserializer.FeedbackListDeserializer;
import com.esites.playtest.sdk.qualifier.Authorized;
import com.esites.playtest.sdk.qualifier.Default;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmList;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger module for all Api classes
 */
@Module(includes = {AppModule.class, PersistenceModule.class})
class ApiModule {

    private static final String TAG = "ApiModule";

    private static final String DATE_FORMAT          = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static final String HEADER_USER_API_TOKEN = "apiToken";
    private static final String HEADER_USER_AGENT      = "User-Agent";



    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder().setDateFormat(DATE_FORMAT)
                                .registerTypeAdapter(RealmList.class, new FeedbackListDeserializer())
                                .create();
    }


    /***************************************************************************************************************
     * Annotated with @Default. Will provide a default webservice without authorization
     ***************************************************************************************************************/

    @Singleton
    @Provides
    @Default
    OkHttpClient provideOkHttpClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(getDefaultInterceptor(context));
        builder.addInterceptor(getLoggingInterceptor());

        return builder.build();
    }

    @Singleton
    @Provides
    @Default
    Retrofit provideRetrofit(@Default OkHttpClient client, Gson gson) {
        return new Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                                     .client(client)
                                     .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                                     .addConverterFactory(GsonConverterFactory.create(gson))
                                     .build();

    }

    @Singleton
    @Provides
    ApiServiceDefault provideApiService(@Default Retrofit retrofit) {
        return retrofit.create(ApiServiceDefault.class);
    }


    /***************************************************************************************************************
     * Annotated with @Authorized. Will provide an authorized webservice
     ***************************************************************************************************************/

    @Singleton
    @Provides
    @Authorized
    OkHttpClient provideAuthOkHttpClient(Context context, PreferenceService preferenceService) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(getDefaultInterceptor(context));
        builder.addInterceptor(getAuthorizedInterceptor(context, preferenceService));
        builder.addInterceptor(getLoggingInterceptor());

        return builder.build();
    }

    @Singleton
    @Provides
    @Authorized
    Retrofit provideAuthRetrofit(@Authorized OkHttpClient client, Gson gson) {
        return new Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                                     .client(client)
                                     .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                                     .addConverterFactory(GsonConverterFactory.create(gson))
                                     .build();

    }

    @Singleton
    @Provides
    ApiService provideAuthApiService(@Authorized Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    /**
     * Authorized interceptor which adds the API token for the user
     * @param context
     * @return
     */
    public Interceptor getAuthorizedInterceptor(final Context context, final PreferenceService preferenceService) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();

                // Create builder
                Request.Builder builder = original.newBuilder()
                                                  .method(original.method(), original.body());

                // Add authorization header with user api token
                addAuthorizationHeader(builder, preferenceService);

                // Build request
                final Request request = builder.build();

                return chain.proceed(request);
            }
        };
    }

    private void addAuthorizationHeader(Request.Builder builder, PreferenceService preferenceService) {
        Log.w(TAG, "addAuthorizationHeader");
        String userApiToken = preferenceService.getUserApiToken();
        if (userApiToken != null) {
            Log.w(TAG, "addAuthorizationHeader: " + userApiToken);
            builder.addHeader(HEADER_USER_API_TOKEN, userApiToken);
        }
    }


    /**
     * Default interceptor which adds a useragent
     * @param context
     * @return
     */
    public Interceptor getDefaultInterceptor(final Context context) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();

                final Request request = original.newBuilder()
                                                .addHeader(HEADER_USER_AGENT, getUserAgent(context))
                                                .method(original.method(), original.body())
                                                .build();
                return chain.proceed(request);
            }
        };
    }

    private static String getUserAgent(Context context) {
        String userAgent = WebSettings.getDefaultUserAgent(context);
        Log.d(TAG, "userAgent: " + userAgent);
        return WebSettings.getDefaultUserAgent(context);
    }

    /**
     * Setup logging interceptor.
     */
    private Interceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

//        if (BuildConfig.DEBUG) {
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        } else {
//            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
//        }

        return logging;
    }

}
