package com.esites.playtest.sdk.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Custom RecyclerView
 */
public class CustomRecyclerView extends RecyclerView {
    private static final String TAG = "RecyclerView";
    private DataListener mDataListener;

    private AdapterDataObserver mDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            updateDataState(false);
        }
    };

    public interface DataListener {
        void onDataPresent();

        void onDataMissing();

        void onDataLoading();
    }

    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        if (getAdapter() != null) {
            getAdapter().unregisterAdapterDataObserver(mDataObserver);
        }
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mDataObserver);
        }
        super.setAdapter(adapter);
        updateDataState(true);
    }

    public void setDataListener(DataListener dataListener) {
        mDataListener = dataListener;
    }

    private void updateDataState(boolean isLoading) {
        Log.i(TAG, "updateDataState");
        if (getAdapter() != null) {
            boolean isEmpty = getAdapter().getItemCount() == 0;

            if (mDataListener != null) {
                if (isEmpty) {
                    if (isLoading) {
                        mDataListener.onDataLoading();
                    } else {
                        mDataListener.onDataMissing();
                    }
                } else {
                    mDataListener.onDataPresent();
                }
            }
        }
    }
}
