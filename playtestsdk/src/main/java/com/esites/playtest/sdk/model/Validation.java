package com.esites.playtest.sdk.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tnanlohy on 09/06/16.
 */
public class Validation {

    @SerializedName("validated")
    private boolean mValidated;

    @SerializedName("code")
    private String mCode;

    @SerializedName("dev")
    private String mDevError;

    public boolean isValidated() {
        return mValidated;
    }

    public void setValidated(boolean validated) {
        mValidated = validated;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getDevError() {
        return mDevError;
    }

    public void setDevError(String devError) {
        mDevError = devError;
    }
}
