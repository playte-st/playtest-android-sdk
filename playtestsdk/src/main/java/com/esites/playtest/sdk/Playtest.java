package com.esites.playtest.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;

import com.esites.playtest.sdk.model.HeartbeatResponse;
import com.esites.playtest.sdk.model.User;
import com.esites.playtest.sdk.model.Validation;
import com.esites.playtest.sdk.txusbubbles.BubbleLayout;
import com.esites.playtest.sdk.txusbubbles.BubblesManager;
import com.esites.playtest.sdk.txusbubbles.OnInitializedCallback;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Playtest public class. Used to init properties
 */
public final class Playtest {

    private static final String TAG = "Playtest";

    public static final String ACTION_ADVERTISING_ID_SET = "ACTION_ADVERTISING_ID_SET";

    private static AppComponent sAppComponent;

    private static BubblesManager sBubblesManager;

    private static BubbleLayout sBubble;

    private static Rect sRect = new Rect();

    private static void initDagger(Application application) {
        sAppComponent = DaggerAppComponent.builder()
                                          .appModule(new AppModule(application))
                                          .persistenceModule(new PersistenceModule())
                                          .apiModule(new ApiModule())
                                          .dataModule(new DataModule())
                                          .build();
    }

    /**
     * Method to get the AppComponent for injecting dagger
     *
     * @return
     */
    static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static class PlaytestBuilder {

        private final Application application;
        private final String      studioApiToken;
        private final String      playtestGUID;
        private       boolean     needsValidation;
        private       boolean     mayAskSystemPermission;

        /**
         * @param application    The application class of your app
         * @param studioApiToken The Playtest studio api token that was provided by Playte.st
         * @param playtestGUID   The Playtest GUID also provided by Playte.st
         */
        public PlaytestBuilder(Application application, String studioApiToken, String playtestGUID) {
            this.application = application;
            this.studioApiToken = studioApiToken;
            this.playtestGUID = playtestGUID;
        }

        /**
         * Enable or disable the validator
         *
         * @param needsValidation
         *
         * @return
         */
        public PlaytestBuilder needsValidation(boolean needsValidation) {
            this.needsValidation = needsValidation;
            return this;
        }

        public PlaytestBuilder mayAskSystemPermission(boolean mayAsk) {
            this.mayAskSystemPermission = mayAsk;
            return this;
        }

        public void build() {
            init(this);
        }
    }

    private static void init(PlaytestBuilder builder) {
        Log.wtf(TAG, "=============================> INITING PLAYTEST");
        // Init Dagger for usage inside the SDK
        initDagger(builder.application);

        // Register the LifecycleCallbackHelper
        builder.application.registerActivityLifecycleCallbacks(LifecycleHelper.getInstance());

        // Save values in preferences
        PreferenceService preferenceService = getAppComponent().getPreferenceService();
        preferenceService.studioApiToken().set(builder.studioApiToken);
        preferenceService.playtestGuid().set(builder.playtestGUID);
        Log.i(TAG, "Needs validation? " + builder.needsValidation);
        preferenceService.needsValidation().set(builder.needsValidation);

        DisplayMetrics metrics = builder.application.getResources().getDisplayMetrics();
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;
        sRect.set(0, 0, widthPixels, heightPixels);

        // Ask Marshmallow permission
        //TODO: We might leave this up to the developers by adding mayAskForPermission boolean that can be set from the Builder
        if (Build.VERSION.SDK_INT >= 23 && !Settings.canDrawOverlays(builder.application)) {
            Intent intent = new Intent(builder.application, MarshmallowPermissionActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            builder.application.startActivity(intent);
        } else {
            finishInit(builder.application);
        }
    }

    static void finishInit(Context context) {
        Log.w(TAG, "finishInit....");
        // Only create advertisingId whenever we have no advertisingId
        PreferenceService preferenceService = getAppComponent().getPreferenceService();
        if (preferenceService.getDeviceUuid() == null) {
            Log.w(TAG, "finishInit no device uuid so start advertisingId service");
            startAdvertisingIdService(context);
        } else {
            Log.w(TAG, "finishInit Call startup");
            startUp(context, preferenceService);
        }
    }

    static void startAdvertisingIdService(Context context) {
        Log.w(TAG, "Starting advertisingId service");
        context.startService(new Intent(context, AdvertisingIdService.class));
    }

    static void startUp(Context context, PreferenceService preferenceService) {
        if (preferenceService.getNeedsValidation()) {
            Log.w("DoneReceiver", "Needs validation was true");
            Playtest.validate(context, preferenceService);
        } else {
            Log.w("DoneReceiver", "Creating session was true");
            Playtest.createSession(context, preferenceService);
        }
    }

    /**************************************************************************************************************
     * Validation stuff
     **************************************************************************************************************/

    static void validate(final Context context, final PreferenceService preferenceService) {
        String deviceUuid = preferenceService.deviceUuid().get();
        if (deviceUuid == null) {
            Log.e(TAG, "validate and advertisingId was null");
            startAdvertisingIdService(context);
            return;
        }
        Log.v(TAG, "deviceUuid: " + deviceUuid);

        String playtestGuid = preferenceService.getPlaytestGuid();
        String studioApiToken = preferenceService.getStudioApiToken();
        String secret = String.valueOf(System.currentTimeMillis());

        String toBeEncrypted = playtestGuid + studioApiToken + secret + deviceUuid;
        String md5Hash = md5(toBeEncrypted);

        // Validate
        ApiServiceDefault apiServiceDefault = getAppComponent().getApiServiceDefault();
        apiServiceDefault.validate(playtestGuid, studioApiToken, deviceUuid, md5Hash, secret)
                         .enqueue(new Callback<Validation>() {
                             @Override
                             public void onResponse(Call<Validation> call, Response<Validation> response) {
                                 Log.w(TAG, "response.isSuccess: " + response.isSuccessful());
                                 Log.w(TAG, "response.code: " + response.code());
                                 Log.w(TAG, "response.body: " + response.body());
                                 if (response.isSuccessful()) {
                                     handleSuccessfulValidation(response.body(), preferenceService, context);
                                 } else {
                                     try {
                                         Validation validation = (Validation) sAppComponent.getRetrofitInstance()
                                                                                           .responseBodyConverter(
                                                                                                   Validation.class,
                                                                                                   Validation.class.getAnnotations())
                                                                                           .convert(
                                                                                                   response.errorBody());
                                         handleValidationError(context, validation, response.code());
                                     } catch (IOException e) {
                                         e.printStackTrace();
                                     }
                                 }
                             }

                             @Override
                             public void onFailure(Call<Validation> call, Throwable t) {
                                 Log.e(TAG, "onFailure validating", t);
                                 handleValidationError(context, null, 0);
                             }
                         });
    }

    private static void handleSuccessfulValidation(Validation validation, PreferenceService preferenceService, Context context) {
        if (validation.isValidated()) {
            User user = preferenceService.user().get();
            // We have a user
            if (user != null) {

                // But this user is anonymous, we should let him log in
                if (user.isAnonymous()) {

                    Intent intent = new Intent(context, PlaytestActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("show_login_from_validation", true);
                    context.startActivity(intent);
                } else {
                    // Not anonymous, so immediately create session
                    createSession(context, preferenceService);
                }
            } else {
                createSession(context, preferenceService);
            }

            // Show bubble
            showBubble();
        }
    }

    private static void handleValidationError(Context context, Validation validation, int code) {
        if (validation != null) {
            Log.e(TAG, "Validation error: " + validation.getCode());
            if (code == 400) {
                Log.e(TAG, "Bad request: " + validation.getDevError());
            } else if (code == 403) {
                Log.e(TAG, "Forbidden: " + validation.getCode());
                if (validation.getCode().equals("DEVICE_UNKNOWN")) {

                    Intent intent = new Intent(context, PlaytestActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("show_login_from_validation", true);
                    context.startActivity(intent);
                } else {
                    // TODO: Show message?
                }
            } else {
                Log.e(TAG, "Playtest not found: " + validation.getCode());
            }
        }

        Log.e(TAG, "+=======================> Validation error");

        hideBubble();
    }

    /**
     * function md5 encryption for passwords
     *
     * @param toBeEncrypted
     *
     * @return passwordEncrypted
     */
    private static final String md5(final String toBeEncrypted) {
        try {

            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(toBeEncrypted.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**************************************************************************************************************
     * Create session stuff
     **************************************************************************************************************/

    static void createSession(Context context, final PreferenceService preferenceService) {
        createSession(context, preferenceService, null);
    }

    static void createSession(Context context, final PreferenceService preferenceService, Runnable onActionDone) {

        // Get device uuid. Check if we have one, if we don't, call service to create one and start over
        String deviceUuid = preferenceService.deviceUuid().get();
        if (deviceUuid == null) {
            Log.e(TAG, "create session and advertisingId was null");
            startAdvertisingIdService(context);
            return;
        }
        Log.v(TAG, "deviceUuid: " + deviceUuid);

        // Get Device size
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int heightPixels = metrics.heightPixels;
        int widthPixels = metrics.widthPixels;

        String size = String.valueOf(widthPixels) + "x" + String.valueOf(heightPixels);

        // Create session and set (anonymous) User
        ApiServiceDefault apiServiceDefault = getAppComponent().getApiServiceDefault();
        apiServiceDefault.createSession(preferenceService.getPlaytestGuid(), deviceUuid, size)
                         .enqueue(new Callback<User>() {
                             @Override
                             public void onResponse(Call<User> call, Response<User> response) {
                                 boolean success = response.isSuccessful();
                                 if (success) {
                                     preferenceService.user().set(response.body());
                                     // Show bubble whenever we did not use the validator
                                     if (!preferenceService.getNeedsValidation()) {
                                         showBubble();
                                     }

                                    sendHeartBeat();
                                 } else {
                                     Log.e(TAG, "Error creating session: " + response.errorBody());
                                 }
                             }

                             @Override
                             public void onFailure(Call<User> call, Throwable t) {
                                 Log.e(TAG, "Error creating session: ", t);
                             }
                         });

        if (onActionDone != null) {
            onActionDone.run();
        }
    }

    private static void sendHeartBeat() {
        final Handler handler = new Handler();
        final long delay = 15 * DateUtils.SECOND_IN_MILLIS;
        final ApiService apiService = sAppComponent.getApiService();
        final PreferenceService preferenceService = sAppComponent.getPreferenceService();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.wtf(TAG, "=============> Heart beating!");
                apiService.sendHeartbeat(preferenceService.getSessionId(), System.currentTimeMillis())
                          .enqueue(new Callback<HeartbeatResponse>() {
                              @Override
                              public void onResponse(Call<HeartbeatResponse> call, Response<HeartbeatResponse> response) {
                                  if (response.isSuccessful()) {
                                      Log.v(TAG, "onResponse heartbeat success");
                                      HeartbeatResponse heartbeatResponse = response.body();
                                      int feedbackCount = heartbeatResponse.getUnseenFeedbacks();
                                      if (feedbackCount > 0) {
                                          showBadgeIfNeeded(feedbackCount);
                                      }
                                  } else {
                                      Log.e(TAG, "onResponse heartbeat error: " + response.code());
                                  }
                              }

                              @Override
                              public void onFailure(Call<HeartbeatResponse> call, Throwable t) {
                                  Log.e(TAG, "onFailure heartbeat error", t);
                              }
                          });

                // Send heart beat again
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    /**************************************************************************************************
     * Bubble methods
     **************************************************************************************************/

    /**
     * Initialize all Bubble related content. This includes starting and binding the service
     * Do this whenever we need the Bubble (during app lifecycle).
     *
     * @param context
     */
    static void initBubble(Activity context) {
        // Init bubble
        doInitBubble(context);

        // Add to view
        Log.w(TAG, "===> Adding bubble from initBubble");
        addBubble();

        // hide immediately if we do not have a user
        PreferenceService preferenceService = sAppComponent.getPreferenceService();
        User user = preferenceService.user().get();
        if (user == null) {
            Log.v(TAG, "Hiding bubble because we do not have a user");
            hideBubble();
        }
    }

    static void doInitBubble(final Activity activity) {
        // Init BubbleManager
        sBubblesManager = new BubblesManager.Builder(activity).setInitializationCallback(new OnInitializedCallback() {
            @Override
            public void onInitialized() {
                Log.w(TAG, "====> onInitialized, add bubble. This will only be called once");
                addBubble();
            }
        }).build();
        sBubblesManager.initialize();

        // Init Bubble itself
        if (sBubble == null) {
            sBubble = (BubbleLayout) LayoutInflater.from(activity).inflate(R.layout.layout_bubble, null);
        }

        sBubble.setShouldStickToWall(true);
        sBubble.setOnBubbleClickListener(new BubbleLayout.OnBubbleClickListener() {
            @Override
            public void onBubbleClick(BubbleLayout bubble) {
                handleBubbleClicked(bubble);
            }
        });
    }

    /**
     * Will handle clicks on the bubble
     *
     * @param bubble
     */
    static void handleBubbleClicked(final BubbleLayout bubble) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Context context = bubble.getContext();

                Intent intent = new Intent(context, PlaytestActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("unseen_feedback", bubble.getFeedbackCount());
                context.startActivity(intent);
            }
        }, 100);

        // Hide bubble
        hideBubble();
    }

    private static void showBadgeIfNeeded(int feedbackCount) {
        if (sBubble != null) {
            sBubble.setBadgeCount(feedbackCount);
        }
    }

    static void hideBadgeIfNeeded() {
        if (sBubble != null) {
            sBubble.setBadgeCount(0);
        }
    }

    static void hideBubble() {
        if (sBubble != null) {
            sBubble.hideBubble();
        }
    }

    static void showBubble() {
        if (sBubble != null) {
            sBubble.showBubble();
        } else {
            Log.e(TAG, "Cannot show bubble because sBubble is null");
        }
    }

    static void reInitializeBubble(Activity activity) {
        initBubble(activity);
        hideBubble();
    }

    /**
     * Destroy Bubble
     */
    static void destroyBubble() {
        if (sBubblesManager != null) {
            Log.i(TAG, "Recycling BubblesManager");

            removeBubble();
            sBubblesManager.recycle();
            sBubblesManager = null;
        }

        sBubble = null;
    }

    private static void addBubble() {

        if (sBubble == null || sBubblesManager == null) {
            Log.e(TAG,
                  "Cannot add bubble, sBubble: " + sBubble + " or sBubblesManager: " + sBubblesManager + " is null");
            return;
        }

        sBubblesManager.addBubble(sBubble, sRect.width(), 100);
    }

    private static void removeBubble() {
        if (sBubble == null || sBubblesManager == null) {
            Log.e(TAG,
                  "Cannot remove bubble, sBubble: " + sBubble + " or sBubblesManager: " + sBubblesManager + " is null");
            return;
        }

        sBubblesManager.removeBubble(sBubble);
    }

    public static boolean isBubbleNull() {
        return sBubble == null;
    }

    public static Rect getRect() {
        return sRect;
    }
}
