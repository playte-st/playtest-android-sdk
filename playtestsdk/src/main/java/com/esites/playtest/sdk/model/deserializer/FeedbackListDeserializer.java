package com.esites.playtest.sdk.model.deserializer;

import android.util.Log;

import com.esites.playtest.sdk.model.RealmFeedback;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.realm.RealmList;

/**
 * Created by tnanlohy on 26/05/16.
 */
public class FeedbackListDeserializer implements JsonDeserializer<RealmList<RealmFeedback>> {
    @Override
    public RealmList<RealmFeedback> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Log.v("ListDeserializer", "FeedbackListDeserializer start log: " + json);
        JsonArray content = json.getAsJsonObject()
                                .get("feedbacks")
                                .getAsJsonArray();

        RealmFeedback[] feedback = context.deserialize(content, RealmFeedback[].class);

        return new RealmList<>(feedback);
    }
}
