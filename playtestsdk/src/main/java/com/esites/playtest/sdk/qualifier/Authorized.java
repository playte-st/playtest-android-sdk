package com.esites.playtest.sdk.qualifier;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Qualifier for authorized requests
 */
@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorized {
}
