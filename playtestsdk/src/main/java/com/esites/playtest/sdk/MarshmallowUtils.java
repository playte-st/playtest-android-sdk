package com.esites.playtest.sdk;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

/**
 * Created by tnanlohy on 21/07/16.
 */
public class MarshmallowUtils {

    /**
     * Ask permission
     *
     * @param permission   The permission itself as written in the AndroidManifest
     * @param addRationale Do we need to show extra explanation?
     * @param requestCode  a requestCode for the permission
     *
     * @return boolean if permission was already granted or not
     */
    public static boolean askPermission(final Activity activity, final String permission, boolean addRationale,
                                        String rationaleQuestion, final int requestCode) {

        // First check if we already have permission, if we do, don't do anything
        int givenPermission = ContextCompat.checkSelfPermission(activity, permission);
        Log.w("Permission", "givenPermission: " + givenPermission);

        if (givenPermission == PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        // Check if we need to show some extra explanation for the user.
        // The dialog that asks for permission, will be shown AFTER
        if (addRationale) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                View view = activity.getWindow().getDecorView();
                Snackbar.make(view, rationaleQuestion, Snackbar.LENGTH_LONG)
                        .setAction(R.string.permission_proceed, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String[] permissions = new String[] {permission};
                                ActivityCompat.requestPermissions(activity, permissions, requestCode);
                            }
                        })
                        .show();

                return false;
            }
        }

        String[] permissions = new String[] {permission};
        ActivityCompat.requestPermissions(activity, permissions, requestCode);

        return false;
    }

    public static boolean isPermissionGranted(int grantResult) {
        return (grantResult == PackageManager.PERMISSION_GRANTED);
    }


}
