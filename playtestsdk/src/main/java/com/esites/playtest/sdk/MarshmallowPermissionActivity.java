package com.esites.playtest.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * Created by tnanlohy on 21/07/16.
 */
public class MarshmallowPermissionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_marshmallow_feedback);

        AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle(R.string.permission_title)
                                                                   .setMessage(R.string.permission_system_alert)
                                                                   .setCancelable(false)
                                                                   .setPositiveButton(R.string.permission_proceed,
                                                                                      new DialogInterface.OnClickListener() {
                                                                                          @Override
                                                                                          public void onClick(DialogInterface dialog, int which) {
                                                                                              startPermissionActivity();
                                                                                          }
                                                                                      })
                                                                   .setNegativeButton(R.string.permission_cancel,
                                                                                      new DialogInterface.OnClickListener() {
                                                                                          @Override
                                                                                          public void onClick(DialogInterface dialog, int which) {
                                                                                              dialog.dismiss();
                                                                                              finish();
                                                                                          }
                                                                                      });
        builder.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void startPermissionActivity() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, 0);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Settings.canDrawOverlays(this)) {
            Playtest.finishInit(this);
        } else {
            // We have no saying over the users actions, so don't do anything here :(
        }

        finish();
    }
}
