package com.esites.playtest.sdk;

import com.esites.playtest.sdk.model.User;
import com.esites.playtest.sdk.model.Validation;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * ApiService that performs calls without user authentication
 */
interface ApiServiceDefault {

    @FormUrlEncoded
    @POST("/android/v2/session")
    Call<User> createSession(@Field("playtest") String playTest,
                             @Field("device") String device,
                             @Field("screen") String screen);

    @FormUrlEncoded
    @POST("/android/v2/validator")
    Call<Validation> validate(@Field("playtest") String playTest,
                              @Field("apiToken") String userApiToken,
                              @Field("device") String device,
                              @Field("token") String md5HashToken,
                              @Field("secret") String secret);
}
