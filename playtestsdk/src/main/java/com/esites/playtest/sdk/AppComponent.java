package com.esites.playtest.sdk;

import com.esites.playtest.sdk.qualifier.Default;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Dagger Appcomponent.
 */

@Singleton
@Component(modules = {PersistenceModule.class, ApiModule.class, DataModule.class})
interface AppComponent {
    void inject(PlaytestActivity activity);
    void inject(FeedbackActivity activity);
    void inject(WebviewActivity activity);
    void inject(IdentificationDialogFragment dialogFragment);
    void inject(AdvertisingIdService service);
    void inject(AdvertisingIdDoneReceiver receiver);
    void inject(MarshmallowPermissionActivity activity);

    // Getter methods
    PreferenceService getPreferenceService();
    ApiServiceDefault getApiServiceDefault();
    ApiService getApiService();

    @Default
    Retrofit getRetrofitInstance();
}
