package com.esites.playtest.sdk;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.esites.playtest.sdk.model.User;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.IntBuffer;

import javax.inject.Inject;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by tnanlohy on 17/05/16.
 */
public class PlaytestActivity extends AppCompatActivity implements View.OnClickListener,
        IdentificationDialogFragment.Listener {

    private static final String TAG = "Playtest";
    @Inject
    ApiService mApiService;

    @Inject
    PreferenceService mPreferenceService;

    private View mMenuLayout;
    private boolean mShowLoginOnly;
    private TextView mBadge;
    private int mUnseenFeedback;

    private int width;
    private int height;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playtest);

        Log.wtf(TAG, "==== Build model: " + Build.MODEL);
        Log.wtf(TAG, "==== Build manufacturer: " + Build.MANUFACTURER);
        Log.wtf(TAG, "==== Build DEVICE: " + Build.DEVICE);
        Log.wtf(TAG, "==== Build BRAND: " + Build.BRAND);


        Playtest.getAppComponent().inject(this);

        mMenuLayout = findViewById(R.id.menu_layout);
        mBadge = (TextView) findViewById(R.id.badge_count);
        mUnseenFeedback = getIntent().getIntExtra("unseen_feedback", 0);
        mBadge.setText(String.valueOf(mUnseenFeedback));
        int visibility = mUnseenFeedback > 0 ? View.VISIBLE : View.GONE;
        mBadge.setVisibility(visibility);

        // Only show login
        mShowLoginOnly = getIntent().getBooleanExtra("show_login_from_validation", false);
        if (mShowLoginOnly) {
            mMenuLayout.setVisibility(View.GONE);
            showIdentificationMenu();
        } else {
            User user = mPreferenceService.user().get();
            performStartWithoutValidator(user);
        }


        // Set onClickListeners
        findViewById(R.id.feedback).setOnClickListener(this);
        findViewById(R.id.screenshot).setOnClickListener(this);
        findViewById(R.id.close).setOnClickListener(this);

        Rect rect = Playtest.getRect();
        width = rect.width();
        height = rect.height();
    }



    private void performStartWithoutValidator(User user) {
        if (user != null) {
            if (user.isAnonymous()) {
                mMenuLayout.setVisibility(View.GONE);
                showIdentificationMenu();
            } else {
                showMenu();
            }
        } else {
            Log.e(TAG, "We clicked the button and opened the PlaytestActivity but there was no user and no session yet");

            mMenuLayout.setVisibility(View.GONE);
            Playtest.createSession(this, mPreferenceService, new Runnable() {
                @Override
                public void run() {
                    showIdentificationMenu();
                }
            });
        }
    }

    private void showIdentificationMenu() {
        IdentificationDialogFragment fragment = IdentificationDialogFragment.createInstance(this);
        fragment.show(getSupportFragmentManager(), "identification");
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.feedback) {
            showAllFeedback();
        } else if (id == R.id.screenshot) {
            takeScreenshot();
        } else if (id == R.id.close) {
            closeActivityAndMenu();
        }
    }

    private void closeActivityAndMenu() {
        finish();
        Playtest.showBubble();
    }

    @Override
    public void onBackPressed() {
        closeActivityAndMenu();
    }

    private void showMenu() {
        mMenuLayout.setVisibility(View.VISIBLE);
    }

    private void showAllFeedback() {
        Intent intent = new Intent(this, FeedbackActivity.class);
        startActivity(intent);

        // Check if there was a visible badge, set it to GONE now, because we clicked the all Feedback menu item
        Playtest.hideBadgeIfNeeded();
        mBadge.setVisibility(View.GONE);


//        overridePendingTransition(R.anim.enter_from_bottom, 0);

//        DisplayUtils.createAndReplaceFragment(R.id.content, getSupportFragmentManager(), FeedbackFragment.class,
//                                              null, "feedback_fragment", false);
    }

    private void takeScreenshot() {
        // Just for logs
        LifecycleHelper.getInstance().showActivityList();

        View viewRoot = LifecycleHelper.getInstance().getView();
        if (viewRoot == null) {
            Toast.makeText(this, "Sorry, we do not have a view available to capture", Toast.LENGTH_SHORT).show();
            return;
        }

        Log.w(TAG, "===> viewRoot: " + viewRoot.getClass().getSimpleName());
        if (viewRoot instanceof GLSurfaceView) {
            GLSurfaceView glSurfaceView = (GLSurfaceView) viewRoot;
            captureBitmap(glSurfaceView, new BitmapReadyCallbacks() {
                @Override
                public void onBitmapReady(Bitmap bitmap) {
                    saveBitmap(bitmap);
                }
            });
        } else {
            Bitmap screenshotBitmap = takeNormalScreenshot(viewRoot);
            saveBitmap(screenshotBitmap);
        }
    }

    private Bitmap takeNormalScreenshot(View view) {
        view.setDrawingCacheEnabled(true);
        Bitmap screenshotBitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        return screenshotBitmap;
    }

    private void saveBitmap(Bitmap screenshotBitmap) {
        try {
            // Used for storage on SD card.
            //            String filePath = Environment.getExternalStorageDirectory()
            //                                      .toString() + ;

            String directory = getFilesDir() + "/Playtest/Screenshots/";
            File dir = new File(directory);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            long now = System.currentTimeMillis();
            String name = now + ".png";
            String filePath = directory + name;
            File imageFile = new File(filePath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);

            int quality = 100;
            screenshotBitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            // Start activity with that image
            startActivityWithScreenshot(filePath);

        } catch (Exception e) {
            Log.e(TAG, "Error: ", e);
        }
    }

    private interface BitmapReadyCallbacks {
        void onBitmapReady(Bitmap bitmap);
    }


    private void captureBitmap(GLSurfaceView glSurfaceView, final BitmapReadyCallbacks bitmapReadyCallbacks) {
        glSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                EGL10 egl = (EGL10) EGLContext.getEGL();
                GL10 gl = (GL10)egl.eglGetCurrentContext().getGL();
                Log.w(TAG, "width: " + width + ", height: " + height);
                final Bitmap bitmap = createBitmapFromGLSurface(0, 0, width, height, gl);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bitmapReadyCallbacks.onBitmapReady(bitmap);
                    }
                });
            }
        });
    }

    // from other answer in this question
    private Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl) {

        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            Log.e(TAG, "createBitmapFromGLSurface: " + e.getMessage(), e);
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    private void startActivityWithScreenshot(String filePath) {
        Intent intent = DisplayUtils.createScreenshotIntent(this, filePath);
        startActivity(intent);
    }

    /************************************************************************************
     * Callback methods IdentificationDialogFragment
     ************************************************************************************/

    @Override
    public void onSuccess() {

        if (mShowLoginOnly) {
            Playtest.createSession(this, mPreferenceService, new Runnable() {
                @Override
                public void run() {
                    showMenu();
                }
            });
        } else {
            User user = mPreferenceService.user().get();
            if (user != null) {
                Log.v(TAG, "Success user.displayName: " + user.getDisplayName() + ", name: " + user.getName() + ", anonymous: " + user.isAnonymous());
            } else {
                Log.e(TAG, "Was success, but User is still null!");
            }


            showMenu();
        }
    }

    @Override
    public void onCancelled() {
        closeActivityAndMenu();
    }

}
