package com.esites.playtest.sdk;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by tnanlohy on 09/06/16.
 */
public class AdvertisingIdService extends IntentService {

    @Inject
    PreferenceService mPreferenceService;

    private static final String TAG = "AdvertisingIdService";

    public AdvertisingIdService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Playtest.getAppComponent().inject(this);

        try {
            AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(this);
            info.isLimitAdTrackingEnabled();
            String advertisingId = info.getId();
            Log.v(TAG, "advertisingId: " + advertisingId);

            // Save advertisingId as device uuid
            mPreferenceService.deviceUuid().set(advertisingId);

            Intent actionIntent = new Intent(Playtest.ACTION_ADVERTISING_ID_SET);
            sendBroadcast(actionIntent);



        } catch (IOException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }
}
