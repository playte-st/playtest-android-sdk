package com.esites.playtest.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * The custom View we can draw upon. This will be drawn over the screenshot
 */
class DrawingView extends View {

    private Path mDrawPath;

    private Paint mDrawPaint, mCanvasPaint;

    private Canvas mDrawCanvas;

    private Bitmap mCanvasBitmap;

    /**
     * The color used while drawing
     */
    private int mEditPaintColor = Color.BLACK;

    /**
     * The color used whenever the user is done drawing
     */
    private int mDonePaintColor = Color.RED;

    public interface DrawingViewTouchListener {
        void onActionDown();
        void onActionUp();
    }



    private DrawingViewTouchListener mTouchListener;

    public DrawingView(Context context) {
        super(context);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mDrawPath = new Path();

        mDrawPaint = new Paint();
        mDrawPaint.setColor(mEditPaintColor);
        mDrawPaint.setAntiAlias(true);
        mDrawPaint.setStrokeWidth(20);
        mDrawPaint.setStyle(Paint.Style.STROKE);
        mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
        mDrawPaint.setStrokeCap(Paint.Cap.ROUND);

        mCanvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    public void setDrawingViewTouchListener(DrawingViewTouchListener touchListener) {
        mTouchListener = touchListener;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mCanvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mDrawCanvas = new Canvas(mCanvasBitmap);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDrawPath.moveTo(touchX, touchY);

                // Call back if necessary
                if (mTouchListener != null) {
                    mTouchListener.onActionDown();
                }

                break;
            case MotionEvent.ACTION_MOVE:
                mDrawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                // Set color to red because we are done drawing
                mDrawPaint.setColor(mDonePaintColor);
                mDrawCanvas.drawPath(mDrawPath, mDrawPaint);
                mDrawPath.reset();

                // Now set color back to black
                mDrawPaint.setColor(mEditPaintColor);

                // Call back if necessary
                if (mTouchListener != null) {
                    mTouchListener.onActionUp();
                }
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(mCanvasBitmap, 0, 0, mCanvasPaint);
        canvas.drawPath(mDrawPath, mDrawPaint);
    }
}
