package com.esites.playtest.sdk;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Dagger module for all persistence classes and helpers and such
 */
@Module(includes = {AppModule.class})
class PersistenceModule {

    @Singleton
    @Provides
    RealmConfiguration provideRealmConfiguration(Context context) {
        return new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                //.schemaVersion(1L)
                .build();

        // FIXME
        // STOPSHIP: 23/12/15 remove "deleteRealmIfMigrationNeeded()" and set "schemaVersion(1L)"
        // FIXME
    }

    @Singleton
    @Provides
    Realm provideRealm(RealmConfiguration configuration) {
        return Realm.getInstance(configuration);
    }

    @Singleton
    @Provides
    PreferenceService providePreferenceService(Context context) {
        return new PreferenceService(context);
    }

}
