package com.esites.playtest.sdk;

import com.esites.playtest.sdk.model.HeartbeatResponse;
import com.esites.playtest.sdk.model.RealmFeedback;
import com.google.gson.JsonElement;

import io.realm.RealmList;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * ApiService that performs call with user authentication
 */

interface ApiService {

    @GET("/api/v2/feedbacks/{playtestID}")
    Call<RealmList<RealmFeedback>> getFeedback(@Path("playtestID") String playTestId);

    @Multipart
    @POST("/android/v2/feedback")
    Call<JsonElement> postFeedback(@Part("screenshot\"; filename=\"screenshot.png\"") RequestBody file,
                                   @Part("playtestSession") RequestBody session,
                                   @Part("feedback") RequestBody feedback);

    @Multipart
    @POST("/android/v2/feedback")
    Call<JsonElement> postFeedbackNoScreenshot(@Part("playtestSession") RequestBody session,
                            @Part("feedback") RequestBody feedback);

    @POST("/api/v2/feedbackvotes/{feedbackID}")
    Call<JsonElement> like(@Path("feedbackID") String feedbackId);

    @FormUrlEncoded
    @POST("/android/v2/heartbeat")
    Call<HeartbeatResponse> sendHeartbeat(@Field("playtestSession") String playtestSession,
                                          @Field("timestamp") long timeStamp);

}
