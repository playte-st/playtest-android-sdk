package com.esites.playtest.demo;

import android.app.Application;

import com.esites.playtest.sdk.Playtest;

/**
 * Created by tnanlohy on 17/05/16.
 */
public class DemoApplication extends Application {

    private static final String PLAYTEST_GUID    = "3863b2e6-5b89-4bb3-9c24-18b72dbee0af";
    private static final String STUDIO_API_TOKEN = "1617c6fa-9bb0-4303-99d0-8f0ab51b649e";

    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize the Playtest SDK
        Playtest.PlaytestBuilder builder = new Playtest.PlaytestBuilder(this, STUDIO_API_TOKEN, PLAYTEST_GUID)
                .needsValidation(false);

        builder.build();
    }
}
